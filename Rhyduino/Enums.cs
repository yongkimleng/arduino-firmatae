﻿//  Copyright © 2009-2010 by Rhy A. Mednick
//  All rights reserved.
//  http://rhyduino.codeplex.com
//  
//  Redistribution and use in source and binary forms, with or without modification, 
//  are permitted provided that the following conditions are met:
//  
//  * Redistributions of source code must retain the above copyright notice, this list 
//    of conditions and the following disclaimer.
//  
//  * Redistributions in binary form must reproduce the above copyright notice, this 
//    list of conditions and the following disclaimer in the documentation and/or other 
//    materials provided with the distribution.
//  
//  * Neither the name of Rhy A. Mednick nor the names of its contributors may be used 
//    to endorse or promote products derived from this software without specific prior 
//    written permission.
//  
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
//  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
namespace Rhyduino
{
    /// <summary>
    ///   The values defining the different types of request messages.
    /// </summary>
    public enum RequestMessageType
    {
        /// <summary>
        ///   Request the reporting of an analog pin (Code: 0xC0)
        /// </summary>
        ReportAnalogPin = 0xC0,
        /// <summary>
        ///   Request the reporting of a digital port (Code: 0xD0)
        /// </summary>
        ReportDigitalPort = 0xD0,
        /// <summary>
        ///   Request a change to the pin mode (Code: 0xF4)
        /// </summary>
        SetPinMode = 0xF4,
        /// <summary>
        ///   Request a remote Firmata system reset (Code: 0xFF)
        /// </summary>
        SystemReset = 0xFF,
        /// <summary>
        ///   Request the remote Firmata protocol version report (Code: 0x79)
        /// </summary>
        ProtocolVersion = 0x79,
        /// <summary>
        ///   Request a digital port value change (Code: 0x90)
        /// </summary>
        DigitalWrite = 0x90,
        /// <summary>
        ///   Request an analog pin value change (Code: 0xE0)
        /// </summary>
        AnalogWrite = 0xE0,
        /// <summary>
        ///   Request a change to the sampling interval (Code: 0x7A)
        /// </summary>
        SetSamplingInterval = 0x7A,
        /// <summary>
        ///   Uninitialized
        /// </summary>
        None = 0x00,
        /// <summary>
        ///   Request to configure a pin for controlling a servo (Code: 0x70)
        /// </summary>
        ServoConfig = 0x70
    }

    /// <summary>
    ///   The values defining the different types of response messages.
    /// </summary>
    public enum ResponseMessageType
    {
        /// <summary>
        ///   The beginning of a sysex block (Code: 0xF0)
        /// </summary>
        SysexStart = 0xF0,
        /// <summary>
        ///   The end of a sysex block (Code: 0xF7)
        /// </summary>
        SysexEnd = 0xF7,
        /// <summary>
        ///   Protocol version notification header (Code: 0x79)
        /// </summary>
        ProtocolVersion = 0x79,
        /// <summary>
        ///   Analog value notification header (Code: 0xE0)
        /// </summary>
        AnalogValue = 0xE0,
        /// <summary>
        ///   Digital value notification header (Code: 0x90)
        /// </summary>
        DigitalValue = 0x90,
        /// <summary>
        ///   Uninitialized
        /// </summary>
        None = 0x00
    }

    /// <summary>
    ///   The valid I/O states for digital pins.
    /// </summary>
    public enum PinMode
    {
        /// <summary>
        ///   Put pin into input mode (Code: 0)
        /// </summary>
        Input = 0,
        /// <summary>
        ///   Put pin into digitial output mode (Code: 1)
        /// </summary>
        Output = 1,
        /// <summary>
        ///   Put pin into analog output mode (Code: 2)
        /// </summary>
        Analog = 2,
        /// <summary>
        ///   Put pin into pulse width modulation output mode (Code: 3)
        /// </summary>
        Pwm = 3,
        /// <summary>
        ///   Put pin into servo control mode (Code: 4)
        /// </summary>
        Servo = 4,
        /// <summary>
        ///   The pin mode enumeration is not initialized.
        /// </summary>
        None = 0xFF
    }

    /// <summary>
    ///   The valid value states for a digital pin.
    /// </summary>
    public enum DigitalPinValue
    {
        ///<summary>
        ///  The pin reports a HIGH state.
        ///</summary>
        High = 0x01,
        ///<summary>
        ///  The pin reports a LOW state.
        ///</summary>
        Low = 0x00,
        ///<summary>
        ///  The value cannot be determined due to the state of the pin.
        ///</summary>
        None = 0xFF
    }

    /// <summary>
    ///   Extended Sysex mesasge types
    /// </summary>
    public enum SysexType
    {
        /// <summary>
        ///   Eeprom write
        /// </summary>
        WRITE_EEPROM_DATA = 0x50,

        /// <summary>
        ///   Eeprom read
        /// </summary>
        READ_EEPROM_DATA = 0x51
        
        // TODO
        //WRITE_SERIAL_DATA = 0x52,
        //READ_SERIAL_DATA = 0x53
    }
}
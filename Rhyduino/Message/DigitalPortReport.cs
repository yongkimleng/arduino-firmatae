﻿//  Copyright © 2009-2010 by Rhy A. Mednick
//  All rights reserved.
//  http://rhyduino.codeplex.com
//  
//  Redistribution and use in source and binary forms, with or without modification, 
//  are permitted provided that the following conditions are met:
//  
//  * Redistributions of source code must retain the above copyright notice, this list 
//    of conditions and the following disclaimer.
//  
//  * Redistributions in binary form must reproduce the above copyright notice, this 
//    list of conditions and the following disclaimer in the documentation and/or other 
//    materials provided with the distribution.
//  
//  * Neither the name of Rhy A. Mednick nor the names of its contributors may be used 
//    to endorse or promote products derived from this software without specific prior 
//    written permission.
//  
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
//  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
using System;
using System.Globalization;

namespace Rhyduino.Message
{
    /// <summary>
    ///   Class containing the data contained in an encoded Firmata digital 
    ///   port report message.
    /// </summary>
    public class DigitalPortReport : FirmataMessage
    {
        #region Propertie(s)

        /// <summary>
        ///   The port being reported.
        /// </summary>
        public int Port { get; protected set; }

        /// <summary>
        ///   The value of the reported port.
        /// </summary>
        public int Value { get; protected set; }

        #endregion

        #region Constructor(s)

        /// <summary>
        ///   Creates and initializes a DigitalPortReport object from an
        ///   encoded Firmata digital port report message.
        /// </summary>
        /// <param name = "message">The encoded message.</param>
        public DigitalPortReport(byte[] message) : base(message)
        {
            if (message == null)
            {
                throw new ArgumentNullException("message");
            }

            // check format of message
            if (message.Length != 3)
            {
                throw new FormatException("A digital pin report message is always 3-bytes long.");
            }

            // mask off control portion of message byte
            var control = (byte) (message[0] & 0xF0);
            // verify control flag
            if (control != (byte) ResponseMessageType.DigitalValue)
            {
                throw new FormatException(
                    String.Format(CultureInfo.CurrentCulture,
                                  "Message was not in expected format. Control flag mismatch."));
            }

            // read port number by stripping away control flag
            Port = (message[0] & 0x0F);

            // Mask off bits that could contain data (per the format spec)
            message[1] &= 0x7F;
            message[2] &= 0x7F;
            // Combine the bytes into an integer value
            Value = message[1] | (message[2] << 7);
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///   Converts the object to a meaninful string representation.
        /// </summary>
        /// <returns>A string stating the [port number] and value.</returns>
        public override string ToString()
        {
            return String.Format(CultureInfo.CurrentCulture, "DigitalPortReport: [{0}] 0x{1:X2}", Port, Value);
        }

        #endregion
    }
}